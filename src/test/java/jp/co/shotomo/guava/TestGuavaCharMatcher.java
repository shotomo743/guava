package jp.co.shotomo.guava;

import com.google.common.base.CharMatcher;
import org.junit.Test;

import static org.junit.Assert.assertThat;
import static org.hamcrest.core.Is.is;

/**
 * Created by shotomo on 2015/06/24.
 * Editor: IntelliJ 14.1.1
 * JDK Version: 1.8.0_40
 */
public class TestGuavaCharMatcher
{
    @Test
    public void testCharMatchers()
    {
        /* 注：全角文字は2文字としてカウントされます。 */
        String str = "abc123あいう!?  ";

        // 全一致
        CharMatcher anyMathcer = CharMatcher.ANY;
        assertThat(anyMathcer.countIn(str), is(16));

        // 数値のみ
        CharMatcher digitMathcer = CharMatcher.DIGIT;
        assertThat(digitMathcer.countIn(str), is(3));

        // 全不一致
        CharMatcher noneMathcer = CharMatcher.NONE;
        assertThat(noneMathcer.countIn(str), is(0));

        // 半角文字
        CharMatcher singleMatcher = CharMatcher.SINGLE_WIDTH;
        assertThat(singleMatcher.countIn(str), is(10));

        // 空白文字
        CharMatcher whiteMatcher = CharMatcher.WHITESPACE;
        assertThat(whiteMatcher.countIn(str), is(2));

        // 指定した文字だけ
        CharMatcher isMathcer = CharMatcher.is('a');
        assertThat(isMathcer.countIn(str), is(1));

        // 指定した文字以外
        CharMatcher isNotMatcher = CharMatcher.isNot('a');
        assertThat(isNotMatcher.countIn(str), is(15));

        // 指定した文字のいずれか
        CharMatcher anyOfMatcher = CharMatcher.anyOf("ab12!");
        assertThat(anyOfMatcher.countIn(str), is(5));

        // 指定した文字のどれでもない
        CharMatcher noneOfMatcher = CharMatcher.noneOf("ab12!");
        assertThat(noneOfMatcher.countIn(str), is(11));

        // 小文字英字
        CharMatcher lowerMathcer = CharMatcher.JAVA_LOWER_CASE;
        assertThat(lowerMathcer.countIn(str), is(3));

        // 大文字英字
        CharMatcher upperMathcer = CharMatcher.JAVA_UPPER_CASE;
        assertThat(upperMathcer.countIn(str), is(0));

        // 範囲
        CharMatcher inRangeMatcher = CharMatcher.inRange('a', 'z');
        assertThat(inRangeMatcher.countIn(str), is(3));

        // CharMatcherをAND結合
        CharMatcher andMatcher = inRangeMatcher.and(anyOfMatcher);
        assertThat(andMatcher.countIn(str), is(2));

        // CharMatcherをOR結合
        CharMatcher orMatcher = inRangeMatcher.or(digitMathcer);
        assertThat(orMatcher.countIn(str), is(6));

        // 条件を逆にする
        CharMatcher negateMatcher = anyMathcer.negate();
        assertThat(negateMatcher.countIn(str), is(0));

        // 条件を自作
        CharMatcher myMathcer = CharMatcher.forPredicate(c -> 'a' <= c && c <= 'z');
        assertThat(myMathcer.countIn(str), is(3));
    }

    @Test
    public void testIndex()
    {
        /**
         * int indexIn(CharSequence)
         * int indexIn(CharSequence, int)
         * int lastIndexIn(CharSequence)
         * int countIn(CharSequence)
         */
    }

    @Test
    public void testMatches()
    {
        /**
         * boolean matches(char)
         * 指定した文字がCharMatcherを満たすか否かを返します。
         */
        assertThat(CharMatcher.JAVA_LOWER_CASE.matches('a'), is(true));
        assertThat(CharMatcher.JAVA_LOWER_CASE.matches('A'), is(false));

        /**
         * boolean	matchesAllOf(CharSequence)
         * 指定した文字列の全ての文字がCharMatcherを満たすか否かを返します。
         */
        assertThat(CharMatcher.JAVA_LOWER_CASE.matchesAllOf("abcdefg"), is(true));
        assertThat(CharMatcher.JAVA_LOWER_CASE.matchesAllOf("Abcdefg"), is(false));

        /**
         * boolean	matchesAnyOf(CharSequence)
         * 指定した文字列のいずれかの文字がCharMatcherを満たすか否かを返します。
         */
        assertThat(CharMatcher.JAVA_LOWER_CASE.matchesAnyOf("abcdefg"), is(true));
        assertThat(CharMatcher.JAVA_LOWER_CASE.matchesAnyOf("Abcdefg"), is(true));
        assertThat(CharMatcher.JAVA_LOWER_CASE.matchesAnyOf("ABCDEFG"), is(false));

        /**
         * boolean	matchesAnyOf(CharSequence)
         * 指定した文字列の全ての文字がCharMatcherを満たさないか否かを返します。
         */
        assertThat(CharMatcher.JAVA_LOWER_CASE.matchesNoneOf("abcdefg"), is(false));
        assertThat(CharMatcher.JAVA_LOWER_CASE.matchesNoneOf("Abcdefg"), is(false));
        assertThat(CharMatcher.JAVA_LOWER_CASE.matchesNoneOf("ABCDEFG"), is(true));
    }

    @Test
    public void testRemove()
    {
        /**
         * String removeFrom(CharSequence)
         * CharMatcherを満たす文字を削除した文字列を返します。
         */
        assertThat(CharMatcher.anyOf(" !").removeFrom("   h e l l o   w o r l d ! !   "), is("helloworld"));
    }


    @Test
    public void testRetain()
    {
        /**
         * String retainFrom(CharSequence)
         * CharMatcherを満たす文字だけを残した文字列を返します。
         */
        assertThat(CharMatcher.anyOf("abc").retainFrom("abcdedcbabcde"), is("abccbabc"));
        assertThat(CharMatcher.anyOf("13579").retainFrom("2147483647"), is("1737"));
    }

    @Test
    public void testTrim()
    {
        /**
         * String trimFrom(CharSequence)
         * CharMatcherを満たす前後の文字を削除した文字列を返します。
         */
        assertThat(CharMatcher.is(' ').trimFrom("   h e l l o   w o r l d ! !   "), is("h e l l o   w o r l d ! !"));
        assertThat(CharMatcher.is('"').trimFrom("\"trimFrom(\"str\")\""), is("trimFrom(\"str\")"));

        /**
         * String trimLeadingFrom(CharSequence)
         * CharMatcherを満たす前方の文字を削除した文字列を返します。
         */
        assertThat(CharMatcher.is(' ').trimLeadingFrom("   h e l l o   w o r l d ! !   "), is("h e l l o   w o r l d ! !   "));
        assertThat(CharMatcher.is('"').trimLeadingFrom("\"trimFrom(\"str\")\""), is("trimFrom(\"str\")\""));

        /**
         * String trimTrailingFrom(CharSequence)
         * CharMatcherを満たす後方の文字を削除した文字列を返します。
         */
        assertThat(CharMatcher.is(' ').trimTrailingFrom("   h e l l o   w o r l d ! !   "), is("   h e l l o   w o r l d ! !"));
        assertThat(CharMatcher.is('"').trimTrailingFrom("\"trimFrom(\"str\")\""), is("\"trimFrom(\"str\")"));
    }

    @Test
    public void testReplace()
    {
        /**
         * String replaceFrom(CharSequence, char)
         * String replaceFrom(CharSequence, CharSequence)
         * CharMatcherを満たす文字を指定した文字または文字列で置換した文字列を返します。
         */
        assertThat(CharMatcher.is(' ').replaceFrom("   h e l l o   w o r l d ! !   ", '.'), is("...h.e.l.l.o...w.o.r.l.d.!.!..."));
        assertThat(CharMatcher.is('o').replaceFrom("google", "ooo"), is("goooooogle"));

        /**
         * String collapseFrom(CharSequence, char)
         * CharMatcherを満たす文字を指定した文字または文字列で置換した文字列を返します。
         * 指定した文字が連続して存在する場合は、それを1つとして扱います。
         */
        assertThat(CharMatcher.is(' ').collapseFrom("   h e l l o   w o r l d ! !   ", '.'), is(".h.e.l.l.o.w.o.r.l.d.!.!."));
        assertThat(CharMatcher.anyOf(" #").collapseFrom("##### h e l l o   w o r l d ! ! #####", '.'), is(".h.e.l.l.o.w.o.r.l.d.!.!."));

        /**
         * String trimAndCollapseFrom(CharSequence, char)
         * CharMatcherを満たす前後の文字を削除し、それ以外は指定した文字で置換した文字列を返します。
         * 指定した文字が連続して存在する場合は、それを1つとして扱います。
         */
        assertThat(CharMatcher.is(' ').trimAndCollapseFrom("   h e l l o   w o r l d ! !   ", '.'), is("h.e.l.l.o.w.o.r.l.d.!.!"));
        assertThat(CharMatcher.anyOf(" #").trimAndCollapseFrom("##### h e l l o   w o r l d ! ! #####", '.'), is("h.e.l.l.o.w.o.r.l.d.!.!"));
    }
}
